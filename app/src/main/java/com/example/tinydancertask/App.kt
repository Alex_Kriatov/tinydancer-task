package com.example.tinydancertask

import android.app.Application
import com.codemonkeylabs.fpslibrary.TinyDancer

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        TinyDancer.create().show(this)
        Thread(ObjectCreationRunnable()).start()
    }
}

class ObjectCreationRunnable : Runnable {
    private val createdObjects = ArrayList<String>()
    override fun run() {
        while (true) {
            if (createdObjects.size > 10000000) createdObjects.clear()
            createdObjects.add("")
        }
    }
}